#!/bin/bash

SNAP_DIRS="/home/oemunoz/Documents/IBM/ELK/snaps/logstash/snaps"

clear
reset
docker run -it --rm -v $PWD/"logstash":/config-dir:ro -v /mnt/autofs/gluster/oemunoz/Documents/data/snap_data:/snap_data:ro logstash logstash -w 1 -f /config-dir/logstash_all_beat.conf
